package ru.surindev.profitestapp.Presenter;

import com.vk.sdk.VKSdk;

import ru.surindev.profitestapp.Utility.ConnectionChecker;
import ru.surindev.profitestapp.View.IAuthView;

public class AuthorizationPresenter extends ConnectionChecker implements IAuthPresenter {
    private IAuthView view;

    @Override
    public void attachView(IAuthView view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void login() {
        if (super.checkConnection(view.getConnectionManager())){
            if (view.getLoginPreference() & VKSdk.isLoggedIn()){
                view.showFriendsList();
            }else{
                view.writeLoginPreference(true);
                view.vkLogin();
            }
        }else{
            view.showLoginError();
        }
    }

    @Override
    public void onLoginSuccess() {
        view.showFriendsList();
    }

    @Override
    public void onLoginError() {
        view.showLoginError();
    }

}
