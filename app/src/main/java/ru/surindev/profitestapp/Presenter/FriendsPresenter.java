package ru.surindev.profitestapp.Presenter;

import java.util.List;

import ru.surindev.profitestapp.DataContainers.ImageCache;
import ru.surindev.profitestapp.DataContainers.Friend;
import ru.surindev.profitestapp.Model.IFriendModel;
import ru.surindev.profitestapp.Utility.ConnectionChecker;
import ru.surindev.profitestapp.View.IFriendsView;

public class FriendsPresenter extends ConnectionChecker implements IFriendsPresenter,
        IFriendModel.LoadFriendsCallback, IFriendModel.LoadFullPicCallback, IFriendModel.LoadThumbCallback {
    private IFriendModel model;
    private IFriendsView view;
    private ImageCache cache;

    public FriendsPresenter(IFriendModel model, int cacheSize) {
        this.model = model;
        cache = new ImageCache(cacheSize);
    }

    public void attachView(IFriendsView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void friendsViewReady(){
        model.loadFriends(this);
    }


    public void loadFullSizeImage(int id){
        model.loadFullImage(id, this);
    }


    // Show photo dialog and load full size image
    public void showPhotoDialog(int id){
        if (viewExists()){
            view.showDialogFragment(id);
            loadFullSizeImage(id);
        }

    }

    public void loadThumb(final String url, final int position){
        model.loadThumb(url, this, position);
    }

    @Override
    public void addBitmapToCache(String key, byte[] image) {
        cache.addBitmapToMemoryCache(key, image);
    }

    @Override
    public byte[] getBitmapFromCache(String key) {
        return cache.getBitmapFromMemCache(key);
    }

    public Boolean checkConnection(){
        return super.checkConnection(view.getConnectionManager());
    }

    private boolean viewExists(){
        return view != null;
    }

    @Override
    public void onFriendsLoadSuccess(List<Friend> friends) {
        if (viewExists()){
            view.showData(friends);
        }
    }

    @Override
    public void onFriendsLoadError() {
        if (viewExists()){
            view.showErrorConnection();
        }
    }

    @Override
    public void onFullImageLoad(byte[] bytes) {
        if (viewExists() && view.fragmentExists()){
            if (bytes != null ){
                view.setFragmentImageSuccess(bytes);
            }else{
                view.setFragmentImageError();
            }
        }
    }

    @Override
    public void onThumbLoaded(byte[] image, int position, String url) {
        if (viewExists()){
            if (position < view.firstVisibleItemPosition() ||
                    position > view.lastVisibleItemPosition()){
                cache.addBitmapToMemoryCache(url, image);
            }else{
                view.refreshThumb(position, image, url);
            }
        }
    }
}
