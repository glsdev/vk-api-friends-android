package ru.surindev.profitestapp.Presenter;

import ru.surindev.profitestapp.View.IAuthView;

public interface IAuthPresenter {

    void attachView(IAuthView view);

    void detachView();

    void login();

    void onLoginSuccess();

    void onLoginError();

}
