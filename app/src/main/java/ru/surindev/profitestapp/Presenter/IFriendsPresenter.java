package ru.surindev.profitestapp.Presenter;

import ru.surindev.profitestapp.View.IFriendsView;

public interface IFriendsPresenter {

    void attachView(IFriendsView view);

    void detachView();

    void friendsViewReady();

    void loadFullSizeImage(int id);

    void showPhotoDialog(int id);

    void loadThumb(final String url, final int position);

    void addBitmapToCache(String key, byte[] image);

    byte[] getBitmapFromCache(String key);


}
