package ru.surindev.profitestapp.Utility;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public abstract class ConnectionChecker {
    public boolean checkConnection(ConnectivityManager connectivityManager){
        boolean connected;
        connected = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
        return connected;
    }

}
