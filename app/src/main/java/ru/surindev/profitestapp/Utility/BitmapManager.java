package ru.surindev.profitestapp.Utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class BitmapManager {
    public static byte[] getBytesFromBitmap(Bitmap bitmap, int quality){
        if (bitmap != null){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
            return stream.toByteArray();
        }else return null;

    }

    public static Bitmap getBitmapFromBytes(byte[] bytes){
        if (bytes != null){
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }else return null;

    }

}
