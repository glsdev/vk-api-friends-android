package ru.surindev.profitestapp.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ru.surindev.profitestapp.DataContainers.Friend;
import ru.surindev.profitestapp.Utility.BitmapManager;

public class FriendsModel implements IFriendModel{


    @Override
    public void loadFriends(final LoadFriendsCallback callback){
        //Initialize request
        VKRequest request = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS,
                "id, first_name, last_name,photo_50","order","hints"));
        request.attempts = 5;
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                LoadFriendsTask friendsTask = new LoadFriendsTask(callback);
                friendsTask.execute(response.responseString);
            }

            @Override
            public void onError(VKError error) {
                callback.onFriendsLoadError();
                super.onError(error);
            }
        });
    }


    private static class LoadFriendsTask extends AsyncTask<String, Void, List<Friend>>{

        private final LoadFriendsCallback friendsCallback;

        private LoadFriendsTask(LoadFriendsCallback friendsCallback) {
            this.friendsCallback = friendsCallback;
        }

        @Override
        protected List<Friend> doInBackground(String... strings) {
            List<Friend> friends = new ArrayList<>();
            JSONArray array;
            try {
                array = new JSONObject(strings[0]).getJSONObject("response").getJSONArray("items");
                for (int i = 0; i < array.length(); i++){
                    JSONObject object = array.getJSONObject(i);
                    Friend friend = new Friend();

                    friend.setId(object.getInt("id"));
                    friend.setName(object.getString("first_name"));
                    friend.setLastName(object.getString("last_name"));
                    friend.setPhotoSmall(object.getString("photo_50"));

                    friends.add(friend);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return friends;
        }

        @Override
        protected void onPostExecute(List<Friend> friends) {
            super.onPostExecute(friends);
            if (friendsCallback != null){
                friendsCallback.onFriendsLoadSuccess(friends);
            }

        }
    }

    @Override
    public void loadFullImage(int id, final LoadFullPicCallback callback){
        VKRequest vkRequest = new VKRequest("photos.get",
                VKParameters.from(VKApiConst.OWNER_ID, String.valueOf(id),
                VKApiConst.ALBUM_ID, "profile", VKApiConst.FIELDS, "5.87"));
        vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                LoadImageTask imageTask = new LoadImageTask(callback);
                imageTask.execute(response.responseString);
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                callback.onFullImageLoad(null);
            }
        });

    }

    private static class LoadImageTask extends AsyncTask<String, Void, byte[]>{
        private final LoadFullPicCallback callback;

        private LoadImageTask(LoadFullPicCallback callback) {
            this.callback = callback;
        }

        @Override
        protected byte[] doInBackground(String... strings) {
            String jsonString = strings[0];
            String url = "";
            try {
                JSONObject json = new JSONObject(jsonString);
                JSONObject response = json.getJSONObject("response");
                JSONArray items = response.getJSONArray("items");
                JSONObject latestPhotos = items.getJSONObject(items.length() -1 );
                JSONArray keys = latestPhotos.names();

                for (int i = keys.length() - 2; i > 0; i--){
                    if (keys.getString(i+1).equals("width")){
                        url = latestPhotos.get(keys.getString(i)).toString();
                        break;
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Bitmap icon = null;
            if (!url.equals("")){
                try {
                    InputStream in = new java.net.URL(url).openStream();
                    icon = BitmapFactory.decodeStream(in);
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return BitmapManager.getBytesFromBitmap(icon, 100);
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            super.onPostExecute(bytes);
            callback.onFullImageLoad(bytes);
        }
    }

    @Override
    public void loadThumb(String url, LoadThumbCallback callback, int position){
        LoadThumbnailTask task = new LoadThumbnailTask(callback, position);
        task.execute(url);
    }



    private static class LoadThumbnailTask extends AsyncTask<String, Void, byte[]>{
        private LoadThumbCallback callback;
        private int position;
        private String url;

        private LoadThumbnailTask(LoadThumbCallback callback, int position) {
            this.callback = callback;
            this.position = position;
        }

        @Override
        protected byte[] doInBackground(String... strings) {
            Bitmap icon = null;
            url = strings[0];
            try {
                InputStream in = new java.net.URL(url).openStream();
                icon = BitmapFactory.decodeStream(in);
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return BitmapManager.getBytesFromBitmap(icon, 100);
        }

        @Override
        protected void onPostExecute(byte[] bytes) {
            super.onPostExecute(bytes);
            callback.onThumbLoaded(bytes, position, url);
        }
    }

}
