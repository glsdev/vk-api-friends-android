package ru.surindev.profitestapp.Model;
import java.util.List;

import ru.surindev.profitestapp.DataContainers.Friend;

public interface IFriendModel {

    interface LoadFriendsCallback{
        void onFriendsLoadSuccess(List<Friend> friends);
        void onFriendsLoadError();
    }

    interface LoadFullPicCallback{
        void onFullImageLoad(byte[] image);
    }

    interface LoadThumbCallback{
        void onThumbLoaded(byte[] image, int position, String url);

    }


    void loadFriends(LoadFriendsCallback callback);
    void loadFullImage(int id, LoadFullPicCallback callback);
    void loadThumb(String url, LoadThumbCallback callback, int position);

}
