package ru.surindev.profitestapp.View;

import android.net.ConnectivityManager;

public interface IAuthView {
    void showLoginError();

    void showFriendsList();

    ConnectivityManager getConnectionManager();

    boolean getLoginPreference();

    void writeLoginPreference(boolean value);

    void vkLogin();
}
