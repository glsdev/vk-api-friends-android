package ru.surindev.profitestapp.View;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import ru.surindev.profitestapp.Presenter.AuthorizationPresenter;
import ru.surindev.profitestapp.Utility.ConnectionChecker;
import ru.surindev.profitestapp.R;

public class AuthView extends AppCompatActivity implements IAuthView {
    private Button signIn;
    private AuthorizationPresenter presenter;
    TextView responseText;
    SharedPreferences sp;
    private final String FRIENDS_SCOPE = "friendsScope";

    private final String PREFERENCES_NAME = "preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        init();

    }
    private void init(){
        signIn = findViewById(R.id.launch_login_btn);
        responseText = findViewById(R.id.response_text);
        presenter = new AuthorizationPresenter();
        presenter.attachView(this);
        sp = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.login();
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                presenter.onLoginSuccess();
            }
            @Override
            public void onError(VKError error) {
                presenter.onLoginError();
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void showLoginError(){
        Toast.makeText(this, "Ошибка подключения", Toast.LENGTH_SHORT).show();
    }

    public void showFriendsList(){
        Intent intent = new Intent(AuthView.this, FriendsView.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finishAffinity();

    }


    @Override
    public ConnectivityManager getConnectionManager() {
        return (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public boolean getLoginPreference() {
        return sp.getBoolean(FRIENDS_SCOPE, false);
    }

    @Override
    public void writeLoginPreference(boolean value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(FRIENDS_SCOPE, value);
        editor.apply();
    }


    @Override
    public void vkLogin() {
        VKSdk.login(this, VKScope.FRIENDS);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
