package ru.surindev.profitestapp.View;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.surindev.profitestapp.Decoration.DividerItemDecoration;
import ru.surindev.profitestapp.DataContainers.Friend;
import ru.surindev.profitestapp.Adapters.FriendsAdapter;
import ru.surindev.profitestapp.Model.FriendsModel;
import ru.surindev.profitestapp.Presenter.FriendsPresenter;
import ru.surindev.profitestapp.Fragments.PhotoDialogFragment;
import ru.surindev.profitestapp.R;
import ru.surindev.profitestapp.Utility.BitmapManager;

public class FriendsView extends AppCompatActivity implements IFriendsView {
    private RecyclerView rvFriends;
    public LinearLayoutManager layoutManager;
    private FriendsPresenter presenter;
    private FriendsAdapter adapter;
    private FragmentManager fragmentManager;
    private PhotoDialogFragment photoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_friends);

        init();
    }

    private void init(){
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        final int cacheSize = maxMemory / 8;

        presenter = new FriendsPresenter(new FriendsModel(), cacheSize);
        presenter.attachView(this);

        rvFriends = findViewById(R.id.rvFriends);
        layoutManager = new LinearLayoutManager(this);
        rvFriends.setLayoutManager(layoutManager);

        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecoration(ContextCompat
                .getDrawable(this, R.drawable.divider));
        rvFriends.addItemDecoration(dividerItemDecoration);


        adapter = new FriendsAdapter(new ArrayList<Friend>(), presenter);
        rvFriends.setAdapter(adapter);

        presenter.friendsViewReady();

        fragmentManager = getSupportFragmentManager();

    }

    public void showData(List<Friend> data){
        adapter.setData(data);
    }

    public void showErrorConnection(){
        Toast.makeText(this, "Ошибка соединения с сервером", Toast.LENGTH_SHORT).show();
    }


    public void setFragmentImageSuccess(byte[] bytes){
        photoFragment.setImageBitmap(BitmapManager.getBitmapFromBytes(bytes));
    }

    public void setFragmentImageError(){
        photoFragment.setImageResource();
    }

    public boolean fragmentExists(){
        return photoFragment != null;
    }

    public void showDialogFragment(int id){
        photoFragment = new PhotoDialogFragment();
        photoFragment.setId(id);
        photoFragment.setPresenter(presenter);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right,
                R.anim.enter_from_right, R.anim.exit_to_right);
        transaction.add(android.R.id.content, photoFragment)
                .addToBackStack(null).commit();
    }

    public void refreshThumb(int pos, byte[] image, String key){
        adapter.updateItem(pos, image, key);
    }


    @Override
    public ConnectivityManager getConnectionManager() {
        return (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public int firstVisibleItemPosition() {
        return layoutManager.findFirstVisibleItemPosition();
    }

    @Override
    public int lastVisibleItemPosition() {
        return layoutManager.findLastVisibleItemPosition();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
