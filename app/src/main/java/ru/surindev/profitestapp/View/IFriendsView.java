package ru.surindev.profitestapp.View;
import android.net.ConnectivityManager;

import java.util.List;

import ru.surindev.profitestapp.DataContainers.Friend;

public interface IFriendsView {
    void showData(List<Friend> data);

    void showErrorConnection();

    void showDialogFragment(int id);

    void refreshThumb(int pos, byte[] image, String key);

    ConnectivityManager getConnectionManager();

    int firstVisibleItemPosition();

    int lastVisibleItemPosition();

    void setFragmentImageSuccess(byte[] bytes);

    void setFragmentImageError();

    boolean fragmentExists();


}
