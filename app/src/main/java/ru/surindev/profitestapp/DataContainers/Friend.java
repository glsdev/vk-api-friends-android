package ru.surindev.profitestapp.DataContainers;

public class Friend{
    private int id;
    private String name;
    private String lastName;
    private String photoSmall;

    //Constructors
    public Friend(int id, String name, String secondName, String thumbnail) {
        this.id = id;
        this.name = name;
        this.lastName = secondName;
        this.photoSmall = thumbnail;
    }

    public Friend() {
        id = 0;
        name = "";
        lastName = "";
        photoSmall = "";

    }


    //Getters

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhotoSmall() {
        return photoSmall;
    }



    //Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhotoSmall(String photoSmall) {
        this.photoSmall = photoSmall;
    }



}
