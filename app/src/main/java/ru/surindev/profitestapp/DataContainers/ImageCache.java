package ru.surindev.profitestapp.DataContainers;

import android.util.LruCache;

import ru.surindev.profitestapp.Utility.BitmapManager;

public class ImageCache extends LruCache<String, byte[]>{


    public ImageCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected int sizeOf(String key, byte[] value) {
        return BitmapManager.getBitmapFromBytes(value).getByteCount();
    }

    public void addBitmapToMemoryCache(String key, byte[] bytes) {
        if (getBitmapFromMemCache(key) == null) {
            this.put(key, bytes);
        }
    }

    public byte[] getBitmapFromMemCache(String key) {
        return this.get(key);
    }
}
