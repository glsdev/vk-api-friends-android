package ru.surindev.profitestapp.Fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import ru.surindev.profitestapp.Presenter.FriendsPresenter;
import ru.surindev.profitestapp.R;

public class PhotoDialogFragment extends DialogFragment {
    private FriendsPresenter presenter;
    private ImageView photo;
    private int id;

    public void setPresenter(FriendsPresenter presenter) {
        this.presenter = presenter;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        photo = view.findViewById(R.id.full_size_image);
        photo.setImageResource(R.drawable.spin);
        photo.setScaleType(ImageView.ScaleType.CENTER);
        photo.setClickable(true);
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    public void setImageBitmap(Bitmap bitmap){
        photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
        photo.setImageBitmap(bitmap);
    }

    public void setImageResource(){
        photo.setScaleType(ImageView.ScaleType.CENTER);
        photo.setImageResource(R.drawable.error_connection);
    }

}
