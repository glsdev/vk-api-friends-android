package ru.surindev.profitestapp.Adapters;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

import ru.surindev.profitestapp.DataContainers.Friend;
import ru.surindev.profitestapp.Presenter.FriendsPresenter;
import ru.surindev.profitestapp.R;
import ru.surindev.profitestapp.Utility.BitmapManager;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.MyViewHolder> {
    List<Friend> data;
    private FriendsPresenter presenter;

    public FriendsAdapter(List<Friend> data, FriendsPresenter presenter) {
        this.data = data;
        this.presenter = presenter;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.name.setText(data.get(position).getName());
        holder.secondName.setText(data.get(position).getLastName());
        final Bitmap bitmap = BitmapManager.getBitmapFromBytes(
                presenter.getBitmapFromCache(data.get(position).getPhotoSmall()));
        if ( bitmap == null & presenter.checkConnection()){
            presenter.loadThumb(data.get(position).getPhotoSmall(), position);

        }else{
            holder.thumb.setImageBitmap(bitmap);
        }

        holder.thumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.showPhotoDialog(data.get(position).getId());
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Friend> data) {
        this.data = data;
        notifyDataSetChanged();
    }


    public void updateItem(int position, byte[] image, String key){
        if (image != null){
            presenter.addBitmapToCache(key, image);
            notifyItemChanged(position);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView name, secondName;
        ImageView thumb;
        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.item_name);
            secondName = view.findViewById(R.id.item_secondName);
            thumb = view.findViewById(R.id.item_thumb);

        }
    }


}
